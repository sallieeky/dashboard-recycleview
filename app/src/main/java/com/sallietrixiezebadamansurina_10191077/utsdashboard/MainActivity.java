package com.sallietrixiezebadamansurina_10191077.utsdashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] txt= new String[]{
                "Information", "Account", "Notification", "Important Number"
        };
        int[] gambar = new int[]{
                R.drawable.ic_baseline_info_24,
                R.drawable.ic_baseline_person_24,
                R.drawable.ic_baseline_notifications_none_24,
                R.drawable.ic_baseline_phone_24
        };

        rv = findViewById(R.id.recycleView);
        RvAdapter adapter = new RvAdapter(this, txt, gambar);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this, 2));
    }
}